﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vectori_uniti
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] v1;
            int[] v2;
            Console.WriteLine("Introduceti lungimea pentrul primul vector!");
            if(!int.TryParse(Console.ReadLine(),out int m))
            {
                Console.WriteLine("Formatul nu este acceptat!!");
                Console.ReadKey();
                return;
            }
            v1 = new int[m];
            Console.WriteLine("Introduceti lungimea pentrul al doilea vector!");
            if (!int.TryParse(Console.ReadLine(), out int n))
            {
                Console.WriteLine("Formatul nu este acceptat!!");
                Console.ReadKey();
                return;
            }
            v2 = new int[n];
            PopuleazaRandom(v1);
            PopuleazaRandom(v2);
            Sorteaza(v1);
            Sorteaza(v2);
            Console.WriteLine( "Primul vector sortat este: ");
            Afiseaza(v1);
            Console.WriteLine("Al doilea vector sortat este: ");
            Afiseaza(v2);
            Console.WriteLine("Vectorii uniti:");
            Uneste(v1, v2);
            Console.ReadKey();


        }
        static void Afiseaza(int[] v)
        {
            for(int i=0;i<v.Length;i++)
            {
                Console.Write(v[i]+" ");
            }
            Console.WriteLine();
        }
        static void PopuleazaRandom(int[] v)
        {
            Random rand = new Random();
            for(int i=0;i<v.Length;i++)
            {
                v[i] = rand.Next(100);
            }
        }
        static void Sorteaza(int[] v)
        {
            for(int i=0;i<v.Length-1;i++)
            {
                for(int j=i+1;j<v.Length;j++)
                    if(v[i]>v[j])
                    {
                        int temp = v[i];
                        v[i] = v[j];
                        v[j] = temp;
                    }
            }
        }
        static void Uneste(int[] v1,int[] v2)
        {
            int[] V = new int[v1.Length + v2.Length];
            int p = 0;
            int i = 0;
            int j = 0;
            while(i<v1.Length&& j<v2.Length)
                {
                    if(v1[i]<v2[j])
                    {
                        V[p] = v1[i];
                        p++;
                        i++;
                    }
                    else
                    {
                        V[p] = v2[j];
                        p++;
                        j++;
                    }
                  
                }
            if(i<v1.Length-1)
            {
                while(i<v1.Length)
                {
                    V[p] = v1[i];
                    i++;
                }
            }
           if(j<v2.Length-1)
            {
                while (j <v2.Length)
                {
                    V[p] = v2[j];
                    j++;
                }

            }
            for(p=0;p<V.Length;p++)
            {
                Console.Write(V[p]+" ");
            }
            Console.WriteLine() ;

        }
    }
}
